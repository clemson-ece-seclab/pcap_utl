from pcap_utl import get_arr_times
from time_series import plot_time_series

DIRECTORY_NAME = '.'
STEP_TIME = 1
WINDOW_SIZE = 1


if __name__ == '__main__':
    arr_times = get_arr_times(DIRECTORY_NAME)  # Directory or a specific file. Does not support regx
    if arr_times:
        plot_time_series(arr_times, step_time=STEP_TIME, window_time=WINDOW_SIZE)