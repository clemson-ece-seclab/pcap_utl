#!/usr/bin/env python
import matplotlib.pyplot as plt
from itertools import groupby


def plot_time_series(arrive_times, step_time=1, window_time=1):

    class Window():
        def __init__(self, init_time):
            self.init_time = init_time
            self.count = 0

    if type(arrive_times) != type([]) or len(arrive_times) == 0:
        print('Not a valid input list.')
        return

    if step_time < 1 or window_time < 1:
        print('step_time and window_time must be intigers larger than zero.')
        return

    _t = -float('inf')    # The last window init time
    _windows = []
    time_tags = []
    number_of_packets = []

    for i, t in enumerate(arrive_times):

        if t - _t > step_time:   # Check if need new window
            _windows.append(Window(int(t)))
            _t = int(t)

        if len(_windows) > 0:
            if t - _windows[0].init_time > window_time:  # remove old window
                time_tags.append(_windows[0].init_time)
                number_of_packets.append(_windows[0].count)
                del(_windows[0])

        for i in range(len(_windows)):
            _windows[i].count += 1

    while len(_windows)>0:  # pop all windows after the last packet
        time_tags.append(_windows[0].init_time)
        number_of_packets.append(_windows[0].count)
        del(_windows[0])

    plt.plot(time_tags, number_of_packets)          # Plot
    plt.show()                                      # Show plot
