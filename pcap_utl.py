from network import Pcap
import os


def get_arr_times(file_or_folder_path='.'):
    pkts = _read_pcaps(file_or_folder_path)
    return [_arr_time(pkt) for pkt in pkts] if pkts else None


def _read_pcaps(file_or_folder_path='.'):
    if os.path.isdir(file_or_folder_path):
        return _folder_to_packets(file_or_folder_path)
    elif os.path.isfile(file_or_folder_path):
        return _pcap_file_to_packet_list(file_or_folder_path, msg=True)
    else:
        print('Not a valid directory or file path.')


def _folder_to_packets(folder_name='.'):
    """
    Convert all pcap files in a directory into a list of packets ordered by packet arriving time
    """
    _r = []
    for file_name in os.listdir(path=folder_name):
        _file_path = os.path.join(folder_name, file_name)
        if os.path.isfile(_file_path):
            try:
                _l = _pcap_file_to_packet_list(_file_path)
                _r = _merge(_r, _l)
            except:
                pass
    if len(_r) == 0:
        print('No valid pcap files found.')
        return
    return _r


def _pcap_file_to_packet_list(file_path, msg=False):
    """
    Read pacp file, return a list of packet
    """
    try:
        data = Pcap.from_file(file_path)
        return [packet for packet in data.packets]
    except:
        if msg:
            print('Not a valid pacp file.')
        return


def _arr_time(packet):
    """
    Return packet arrive time.
    Return a float: epochsecond.mircosecond
    Only works for regular microsecond-resolution files
    """
    return packet.ts_sec + packet.ts_usec / 1e6


def _merge(list_a, list_b):
    """
    Merge sort two list of packets according to arrive time
    Expected run time O(len(shorter_list))
    """
    if type(list_a) != type([]):
        list_a = []
    if type(list_b) != type([]):
        list_b = []
    _r = []

    while len(list_a) > 0 and len(list_b) > 0:
        _r.append(list_a.pop() if _arr_time(
            list_a[-1]) >= _arr_time(list_b[-1]) else list_b.pop())
    _r.reverse()
    _r = list_a + list_b + _r
    return _r
